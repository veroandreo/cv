library(datadrivencv)
library(here)
use_datadriven_cv(
  full_name = "Veronica Andreo",
  data_location = "https://docs.google.com/spreadsheets/d/18lzm-jEuWMP9HjAq73Ze6rh_EPQeHBEPNawT924tdis/edit?usp=sharing",
  source_location = "https://gitlab.com/veroandreo/cv",
  output_dir = here(),
  open_files = FALSE
)
