## My pagedown rendered CV

### Structure

This repo contains the source-code and output of my CV built with the [pagedown package](https://pagedown.rbind.io) and a modified version of the 'resume' template by [nstrayer](https://github.com/nstrayer/cv). 

The main files are:

- `cv.rmd`: Source template for the cv, contains a YAML variable `pdf_mode` in the header that changes styles for pdf vs html. 
- `render_cv.r`: R script for rendering both pdf and html version of CV at the same time.
- `cv.html`: The final output of the template when the header variable `PDF_EXPORT` is set to `FALSE`. View it at [veroandreo.gitlab.io/cv](https://veroandreo.gitlab.io/files/cv.html).
- `cv.pdf`: The final cv exported as pdf. 
- `dd_cv.css`: Custom CSS file used to tweak the default 'resume' format from pagedown. 
- `resume.rmd`: Source template for shorter cv. Basically, it filters some entries from the source data stored in a google sheet.
- `resume.html`/`resume.pdf`: The final shorter cv exported as html/pdf.
- `*_es.*`: Spanish version of all the above.

### Source data

Data is stored in a google sheet in separate tabs. The [datadrivencv](https://github.com/nstrayer/datadrivencv) package uses the wonderful [googlesheets4](https://googlesheets4.tidyverse.org/index.html) package to read the data in.


### Want to use the same approach to build your own CV/resume? 

See [datadrivencv](http://nickstrayer.me/datadrivencv/). The easiest way to get going is running these lines in the directory you want to have your CV in: 

```r
devtools::install_github("nstrayer/datadrivencv")
datadrivencv::use_datadriven_cv(full_name = "Your Name")
```

It wil create the base files so you can then customize and build with the instructions in `render_cv.r`.

